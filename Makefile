# Programs
JAVAC?=javac
JAR?=jar

# Release version informtaion
VER=0.1

# dependent libs directory location debug-disable.jar, debug-enable-1.1.jar, hexdump.jar, libdbus-java-2.7.jar
# slf4j-api-1.6.1.jar, slf4j-jdk14-1.6.1.jar, unix.jar, twitter4j-core-2.2.3.jar
DEPLIBDIR?=./lib

#source code directory
SRCDIR=src

all: purpleinterface.jar syncup-$(VER).jar 

purpleinterface.jar: .purpleclasses
	(cd classes; $(JAR) cf ../$@ im/pidgin/purple/*.class)

syncup-$(VER).jar: .classes
	(cd classes; $(JAR) cf ../$@ com/syncup/dbus/*.class com/syncup/twitter4j/oauth/*.class com/syncup/main/*.class)

purpleclasses: .purpleclasses

classes: .classes

.classes: $(SRCDIR)/com/syncup/dbus/*.java $(SRCDIR)/com/syncup/twitter4j/oauth/*.java $(SRCDIR)/com/syncup/main/*.java
	mkdir -p classes
	$(JAVAC) -d classes -cp classes:$(DEPLIBDIR)/debug-disable.jar:$(DEPLIBDIR)/hexdump.jar:$(DEPLIBDIR)/libdbus-java-2.7.jar:$(DEPLIBDIR)/slf4j-api-1.6.1.jar:$(DEPLIBDIR)/unix.jar:$(DEPLIBDIR)/twitter4j-core-2.2.5.jar:../purpleinterface.jar $(SRCDIR)/com/syncup/dbus/*.java $(SRCDIR)/com/syncup/twitter4j/oauth/*.java $(SRCDIR)/com/syncup/main/*.java
	touch .classes
.purpleclasses: $(SRCDIR)/im/pidgin/purple/*.java 
	mkdir -p classes
	$(JAVAC) -d classes -cp classes:$(DEPLIBDIR)/libdbus-java-2.7.jar $(SRCDIR)/im/pidgin/purple/*.java
	touch .purpleclasses

clean:
	rm -rf classes
	rm -f syncup-$(VER).jar purpleinterface.jar .classes .purpleclasses
