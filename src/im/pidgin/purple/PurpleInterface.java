/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package im.pidgin.purple;
import java.util.List;
import java.util.Map;
import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.UInt32;
import org.freedesktop.dbus.exceptions.DBusException;
public interface PurpleInterface extends DBusInterface
{
   public static class AccountConnecting extends DBusSignal
   {
      public final int a;
      public AccountConnecting(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountDisabled extends DBusSignal
   {
      public final int a;
      public AccountDisabled(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountEnabled extends DBusSignal
   {
      public final int a;
      public AccountEnabled(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountSettingInfo extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountSettingInfo(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountSetInfo extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountSetInfo(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountCreated extends DBusSignal
   {
      public final int a;
      public AccountCreated(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountDestroying extends DBusSignal
   {
      public final int a;
      public AccountDestroying(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountAdded extends DBusSignal
   {
      public final int a;
      public AccountAdded(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountRemoved extends DBusSignal
   {
      public final int a;
      public AccountRemoved(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountStatusChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public AccountStatusChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class AccountActionsChanged extends DBusSignal
   {
      public final int a;
      public AccountActionsChanged(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountAliasChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountAliasChanged(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountAuthorizationRequested extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountAuthorizationRequested(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountAuthorizationDenied extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountAuthorizationDenied(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountAuthorizationGranted extends DBusSignal
   {
      public final int a;
      public final int b;
      public AccountAuthorizationGranted(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class AccountErrorChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public AccountErrorChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class AccountSignedOn extends DBusSignal
   {
      public final int a;
      public AccountSignedOn(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountSignedOff extends DBusSignal
   {
      public final int a;
      public AccountSignedOff(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class AccountConnectionError extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public AccountConnectionError(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class BuddyStatusChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public BuddyStatusChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class BuddyPrivacyChanged extends DBusSignal
   {
      public final int a;
      public BuddyPrivacyChanged(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyIdleChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public BuddyIdleChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class BuddySignedOn extends DBusSignal
   {
      public final int a;
      public BuddySignedOn(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddySignedOff extends DBusSignal
   {
      public final int a;
      public BuddySignedOff(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyGotLoginTime extends DBusSignal
   {
      public final int a;
      public BuddyGotLoginTime(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BlistNodeAdded extends DBusSignal
   {
      public final int a;
      public BlistNodeAdded(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BlistNodeRemoved extends DBusSignal
   {
      public final int a;
      public BlistNodeRemoved(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyAdded extends DBusSignal
   {
      public final int a;
      public BuddyAdded(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyRemoved extends DBusSignal
   {
      public final int a;
      public BuddyRemoved(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyIconChanged extends DBusSignal
   {
      public final int a;
      public BuddyIconChanged(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class UpdateIdle extends DBusSignal
   {
      public UpdateIdle(String path) throws DBusException
      {
         super(path);
      }
   }
   public static class BlistNodeExtendedMenu extends DBusSignal
   {
      public final int a;
      public final int b;
      public BlistNodeExtendedMenu(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class BlistNodeAliased extends DBusSignal
   {
      public final int a;
      public final int b;
      public BlistNodeAliased(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class BuddyCapsChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public BuddyCapsChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class CertificateStored extends DBusSignal
   {
      public final int a;
      public final int b;
      public CertificateStored(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class CertificateDeleted extends DBusSignal
   {
      public final int a;
      public final int b;
      public CertificateDeleted(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class CipherAdded extends DBusSignal
   {
      public final int a;
      public CipherAdded(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class CipherRemoved extends DBusSignal
   {
      public final int a;
      public CipherRemoved(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class CmdAdded extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public CmdAdded(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class CmdRemoved extends DBusSignal
   {
      public final int a;
      public CmdRemoved(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SigningOn extends DBusSignal
   {
      public final int a;
      public SigningOn(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SignedOn extends DBusSignal
   {
      public final int a;
      public SignedOn(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SigningOff extends DBusSignal
   {
      public final int a;
      public SigningOff(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SignedOff extends DBusSignal
   {
      public final int a;
      public SignedOff(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class ConnectionError extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ConnectionError(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class Autojoin extends DBusSignal
   {
      public final int a;
      public Autojoin(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class WritingImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public WritingImMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class WroteImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public WroteImMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class SentAttention extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final UInt32 d;
      public SentAttention(String path, int a, int b, int c, UInt32 d) throws DBusException
      {
         super(path, a, b, c, d);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
      }
   }
   public static class GotAttention extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final UInt32 d;
      public GotAttention(String path, int a, int b, int c, UInt32 d) throws DBusException
      {
         super(path, a, b, c, d);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
      }
   }
   public static class SendingImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public SendingImMsg(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class SentImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public SentImMsg(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ReceivingImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final int e;
      public ReceivingImMsg(String path, int a, int b, int c, int d, int e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class ReceivedImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public ReceivedImMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class BlockedImMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final UInt32 d;
      public final UInt32 e;
      public BlockedImMsg(String path, int a, int b, int c, UInt32 d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class WritingChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public WritingChatMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class WroteChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public WroteChatMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class SendingChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final UInt32 c;
      public SendingChatMsg(String path, int a, int b, UInt32 c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class SentChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final UInt32 c;
      public SentChatMsg(String path, int a, int b, UInt32 c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ReceivingChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final int e;
      public ReceivingChatMsg(String path, int a, int b, int c, int d, int e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class ReceivedChatMsg extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public ReceivedChatMsg(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class ConversationCreated extends DBusSignal
   {
      public final int a;
      public ConversationCreated(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class ConversationUpdated extends DBusSignal
   {
      public final int a;
      public final UInt32 b;
      public ConversationUpdated(String path, int a, UInt32 b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class DeletingConversation extends DBusSignal
   {
      public final int a;
      public DeletingConversation(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class BuddyTyping extends DBusSignal
   {
      public final int a;
      public final int b;
      public BuddyTyping(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class BuddyTyped extends DBusSignal
   {
      public final int a;
      public final int b;
      public BuddyTyped(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class BuddyTypingStopped extends DBusSignal
   {
      public final int a;
      public final int b;
      public BuddyTypingStopped(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class ChatBuddyJoining extends DBusSignal
   {
      public final int a;
      public final int b;
      public final UInt32 c;
      public ChatBuddyJoining(String path, int a, int b, UInt32 c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ChatBuddyJoined extends DBusSignal
   {
      public final int a;
      public final int b;
      public final UInt32 c;
      public final UInt32 d;
      public ChatBuddyJoined(String path, int a, int b, UInt32 c, UInt32 d) throws DBusException
      {
         super(path, a, b, c, d);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
      }
   }
   public static class ChatBuddyFlags extends DBusSignal
   {
      public final int a;
      public final int b;
      public final UInt32 c;
      public final UInt32 d;
      public ChatBuddyFlags(String path, int a, int b, UInt32 c, UInt32 d) throws DBusException
      {
         super(path, a, b, c, d);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
      }
   }
   public static class ChatBuddyLeaving extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ChatBuddyLeaving(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ChatBuddyLeft extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ChatBuddyLeft(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ChatInvitingUser extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ChatInvitingUser(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ChatInvitedUser extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ChatInvitedUser(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ChatInvited extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final int e;
      public ChatInvited(String path, int a, int b, int c, int d, int e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class ChatInviteBlocked extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final int e;
      public ChatInviteBlocked(String path, int a, int b, int c, int d, int e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class ChatJoined extends DBusSignal
   {
      public final int a;
      public ChatJoined(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class ChatJoinFailed extends DBusSignal
   {
      public final int a;
      public final int b;
      public ChatJoinFailed(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class ChatLeft extends DBusSignal
   {
      public final int a;
      public ChatLeft(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class ChatTopicChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public ChatTopicChanged(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class ConversationExtendedMenu extends DBusSignal
   {
      public final int a;
      public final int b;
      public ConversationExtendedMenu(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class UriHandler extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public UriHandler(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class Quitting extends DBusSignal
   {
      public Quitting(String path) throws DBusException
      {
         super(path);
      }
   }
   public static class FileRecvAccept extends DBusSignal
   {
      public final int a;
      public FileRecvAccept(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileSendAccept extends DBusSignal
   {
      public final int a;
      public FileSendAccept(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileRecvStart extends DBusSignal
   {
      public final int a;
      public FileRecvStart(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileSendStart extends DBusSignal
   {
      public final int a;
      public FileSendStart(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileSendCancel extends DBusSignal
   {
      public final int a;
      public FileSendCancel(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileRecvCancel extends DBusSignal
   {
      public final int a;
      public FileRecvCancel(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileSendComplete extends DBusSignal
   {
      public final int a;
      public FileSendComplete(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileRecvComplete extends DBusSignal
   {
      public final int a;
      public FileRecvComplete(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class FileRecvRequest extends DBusSignal
   {
      public final int a;
      public FileRecvRequest(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class ImageDeleting extends DBusSignal
   {
      public final int a;
      public ImageDeleting(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class LogTimestamp extends DBusSignal
   {
      public final int a;
      public final long b;
      public final boolean c;
      public LogTimestamp(String path, int a, long b, boolean c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class NetworkConfigurationChanged extends DBusSignal
   {
      public NetworkConfigurationChanged(String path) throws DBusException
      {
         super(path);
      }
   }
   public static class DisplayingEmailNotification extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public DisplayingEmailNotification(String path, int a, int b, int c, int d) throws DBusException
      {
         super(path, a, b, c, d);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
      }
   }
   public static class DisplayingEmailsNotification extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public final int d;
      public final UInt32 e;
      public DisplayingEmailsNotification(String path, int a, int b, int c, int d, UInt32 e) throws DBusException
      {
         super(path, a, b, c, d, e);
         this.a = a;
         this.b = b;
         this.c = c;
         this.d = d;
         this.e = e;
      }
   }
   public static class DisplayingUserinfo extends DBusSignal
   {
      public final int a;
      public final int b;
      public final int c;
      public DisplayingUserinfo(String path, int a, int b, int c) throws DBusException
      {
         super(path, a, b, c);
         this.a = a;
         this.b = b;
         this.c = c;
      }
   }
   public static class PluginLoad extends DBusSignal
   {
      public final int a;
      public PluginLoad(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class PluginUnload extends DBusSignal
   {
      public final int a;
      public PluginUnload(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SavedstatusChanged extends DBusSignal
   {
      public final int a;
      public final int b;
      public SavedstatusChanged(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class SavedstatusAdded extends DBusSignal
   {
      public final int a;
      public SavedstatusAdded(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SavedstatusDeleted extends DBusSignal
   {
      public final int a;
      public SavedstatusDeleted(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class SavedstatusModified extends DBusSignal
   {
      public final int a;
      public SavedstatusModified(String path, int a) throws DBusException
      {
         super(path, a);
         this.a = a;
      }
   }
   public static class PlayingSoundEvent extends DBusSignal
   {
      public final int a;
      public final int b;
      public PlayingSoundEvent(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class IrcSendingText extends DBusSignal
   {
      public final int a;
      public final int b;
      public IrcSendingText(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }
   public static class IrcReceivingText extends DBusSignal
   {
      public final int a;
      public final int b;
      public IrcReceivingText(String path, int a, int b) throws DBusException
      {
         super(path, a, b);
         this.a = a;
         this.b = b;
      }
   }

  public int PurpleAccountsFindAny(String name, String protocol);
  public int PurpleAccountsFindConnected(String name, String protocol);
  public int PurpleBlistNodeIsChat(int node);
  public int PurpleBlistNodeIsBuddy(int node);
  public int PurpleBlistNodeIsContact(int node);
  public int PurpleBlistNodeIsGroup(int node);
  public int PurpleBuddyIsOnline(int buddy);
  public int PurpleBlistNodeHasFlag(int node, int flags);
  public int PurpleBlistNodeShouldSave(int node);
  public int PurpleConnectionIsConnected(int connection);
  public int PurpleConnectionIsValid(int connection);
  public int PurpleConvIm(int conversation);
  public int PurpleConvChat(int conversation);
  public int PurpleAccountNew(String username, String protocol_id);
  public void PurpleAccountDestroy(int account);
  public void PurpleAccountConnect(int account);
  public void PurpleAccountRegister(int account);
  public void PurpleAccountDisconnect(int account);
  public void PurpleAccountNotifyAdded(int account, String remote_user, String id, String alias, String message);
  public void PurpleAccountRequestAdd(int account, String remote_user, String id, String alias, String message);
  public void PurpleAccountRequestCloseWithAccount(int account);
  public void PurpleAccountRequestClose(int ui_handle);
  public void PurpleAccountRequestChangePassword(int account);
  public void PurpleAccountRequestChangeUserInfo(int account);
  public void PurpleAccountSetUsername(int account, String username);
  public void PurpleAccountSetPassword(int account, String password);
  public void PurpleAccountSetAlias(int account, String alias);
  public void PurpleAccountSetUserInfo(int account, String user_info);
  public void PurpleAccountSetBuddyIconPath(int account, String path);
  public void PurpleAccountSetProtocolId(int account, String protocol_id);
  public void PurpleAccountSetConnection(int account, int gc);
  public void PurpleAccountSetRememberPassword(int account, int value);
  public void PurpleAccountSetCheckMail(int account, int value);
  public void PurpleAccountSetEnabled(int account, String ui, int value);
  public void PurpleAccountSetProxyInfo(int account, int info);
  public void PurpleAccountSetPrivacyType(int account, int privacy_type);
  public void PurpleAccountSetStatusTypes(int account, int status_types);
  public void PurpleAccountSetStatusList(int account, String status_id, int active, int attrs);
  public void PurpleAccountClearSettings(int account);
  public void PurpleAccountRemoveSetting(int account, String setting);
  public void PurpleAccountSetInt(int account, String name, int value);
  public void PurpleAccountSetString(int account, String name, String value);
  public void PurpleAccountSetBool(int account, String name, int value);
  public void PurpleAccountSetUiInt(int account, String ui, String name, int value);
  public void PurpleAccountSetUiString(int account, String ui, String name, String value);
  public void PurpleAccountSetUiBool(int account, String ui, String name, int value);
  public int PurpleAccountIsConnected(int account);
  public int PurpleAccountIsConnecting(int account);
  public int PurpleAccountIsDisconnected(int account);
  public String PurpleAccountGetUsername(int account);
  public String PurpleAccountGetPassword(int account);
  public String PurpleAccountGetAlias(int account);
  public String PurpleAccountGetUserInfo(int account);
  public String PurpleAccountGetBuddyIconPath(int account);
  public String PurpleAccountGetProtocolId(int account);
  public String PurpleAccountGetProtocolName(int account);
  public int PurpleAccountGetConnection(int account);
  public String PurpleAccountGetNameForDisplay(int account);
  public int PurpleAccountGetRememberPassword(int account);
  public int PurpleAccountGetCheckMail(int account);
  public int PurpleAccountGetEnabled(int account, String ui);
  public int PurpleAccountGetProxyInfo(int account);
  public int PurpleAccountGetPrivacyType(int account);
  public int PurpleAccountGetActiveStatus(int account);
  public int PurpleAccountGetStatus(int account, String status_id);
  public int PurpleAccountGetStatusType(int account, String id);
  public int PurpleAccountGetStatusTypeWithPrimitive(int account, int primitive);
  public int PurpleAccountGetPresence(int account);
  public int PurpleAccountIsStatusActive(int account, String status_id);
  public List<Integer> PurpleAccountGetStatusTypes(int account);
  public int PurpleAccountGetInt(int account, String name, int default_value);
  public String PurpleAccountGetString(int account, String name, String default_value);
  public int PurpleAccountGetBool(int account, String name, int default_value);
  public int PurpleAccountGetUiInt(int account, String ui, String name, int default_value);
  public String PurpleAccountGetUiString(int account, String ui, String name, String default_value);
  public int PurpleAccountGetUiBool(int account, String ui, String name, int default_value);
  public int PurpleAccountGetLog(int account, int create);
  public void PurpleAccountDestroyLog(int account);
  public void PurpleAccountAddBuddy(int account, int buddy);
  public void PurpleAccountAddBuddies(int account, int buddies);
  public void PurpleAccountRemoveBuddy(int account, int buddy, int group);
  public void PurpleAccountRemoveBuddies(int account, int buddies, int groups);
  public void PurpleAccountRemoveGroup(int account, int group);
  public void PurpleAccountChangePassword(int account, String orig_pw, String new_pw);
  public int PurpleAccountSupportsOfflineMessage(int account, int buddy);
  public int PurpleAccountGetCurrentError(int account);
  public void PurpleAccountClearCurrentError(int account);
  public void PurpleAccountsAdd(int account);
  public void PurpleAccountsRemove(int account);
  public void PurpleAccountsDelete(int account);
  public void PurpleAccountsReorder(int account, int new_index);
  public List<Integer> PurpleAccountsGetAll();
  public List<Integer> PurpleAccountsGetAllActive();
  public int PurpleAccountsFind(String name, String protocol);
  public void PurpleAccountsRestoreCurrentStatuses();
  public void PurpleAccountsSetUiOps(int ops);
  public int PurpleAccountsGetUiOps();
  public void PurpleAccountsInit();
  public void PurpleAccountsUninit();
  public int PurpleBlistNew();
  public void PurpleSetBlist(int blist);
  public int PurpleGetBlist();
  public int PurpleBlistGetRoot();
  public List<Integer> PurpleBlistGetBuddies();
  public int PurpleBlistNodeNext(int node, int offline);
  public int PurpleBlistNodeGetParent(int node);
  public int PurpleBlistNodeGetFirstChild(int node);
  public int PurpleBlistNodeGetSiblingNext(int node);
  public int PurpleBlistNodeGetSiblingPrev(int node);
  public void PurpleBlistShow();
  public void PurpleBlistDestroy();
  public void PurpleBlistSetVisible(int show);
  public void PurpleBlistUpdateBuddyStatus(int buddy, int old_status);
  public void PurpleBlistUpdateNodeIcon(int node);
  public void PurpleBlistUpdateBuddyIcon(int buddy);
  public void PurpleBlistRenameBuddy(int buddy, String name);
  public void PurpleBlistAliasContact(int contact, String alias);
  public void PurpleBlistAliasBuddy(int buddy, String alias);
  public void PurpleBlistServerAliasBuddy(int buddy, String alias);
  public void PurpleBlistAliasChat(int chat, String alias);
  public void PurpleBlistRenameGroup(int group, String name);
  public int PurpleChatNew(int account, String alias, Map<String,String> components);
  public void PurpleChatDestroy(int chat);
  public void PurpleBlistAddChat(int chat, int group, int node);
  public int PurpleBuddyNew(int account, String name, String alias);
  public void PurpleBuddyDestroy(int buddy);
  public void PurpleBuddySetIcon(int buddy, int icon);
  public int PurpleBuddyGetAccount(int buddy);
  public String PurpleBuddyGetName(int buddy);
  public int PurpleBuddyGetIcon(int buddy);
  public int PurpleBuddyGetContact(int buddy);
  public int PurpleBuddyGetPresence(int buddy);
  public int PurpleBuddyGetMediaCaps(int buddy);
  public void PurpleBuddySetMediaCaps(int buddy, int media_caps);
  public void PurpleBlistAddBuddy(int buddy, int contact, int group, int node);
  public int PurpleGroupNew(String name);
  public void PurpleGroupDestroy(int group);
  public void PurpleBlistAddGroup(int group, int node);
  public int PurpleContactNew();
  public void PurpleContactDestroy(int contact);
  public int PurpleContactGetGroup(int contact);
  public void PurpleBlistAddContact(int contact, int group, int node);
  public void PurpleBlistMergeContact(int source, int node);
  public int PurpleContactGetPriorityBuddy(int contact);
  public void PurpleContactSetAlias(int contact, String alias);
  public String PurpleContactGetAlias(int contact);
  public int PurpleContactOnAccount(int contact, int account);
  public void PurpleContactInvalidatePriorityBuddy(int contact);
  public void PurpleBlistRemoveBuddy(int buddy);
  public void PurpleBlistRemoveContact(int contact);
  public void PurpleBlistRemoveChat(int chat);
  public void PurpleBlistRemoveGroup(int group);
  public String PurpleBuddyGetAliasOnly(int buddy);
  public String PurpleBuddyGetServerAlias(int buddy);
  public String PurpleBuddyGetContactAlias(int buddy);
  public String PurpleBuddyGetLocalAlias(int buddy);
  public String PurpleBuddyGetAlias(int buddy);
  public String PurpleBuddyGetLocalBuddyAlias(int buddy);
  public String PurpleChatGetName(int chat);
  public int PurpleFindBuddy(int account, String name);
  public int PurpleFindBuddyInGroup(int account, String name, int group);
  public List<Integer> PurpleFindBuddies(int account, String name);
  public int PurpleFindGroup(String name);
  public int PurpleBlistFindChat(int account, String name);
  public int PurpleChatGetGroup(int chat);
  public int PurpleChatGetAccount(int chat);
  public int PurpleBuddyGetGroup(int buddy);
  public List<Integer> PurpleGroupGetAccounts(int g);
  public int PurpleGroupOnAccount(int g, int account);
  public String PurpleGroupGetName(int group);
  public void PurpleBlistAddAccount(int account);
  public void PurpleBlistRemoveAccount(int account);
  public int PurpleBlistGetGroupSize(int group, int offline);
  public int PurpleBlistGetGroupOnlineCount(int group);
  public void PurpleBlistLoad();
  public void PurpleBlistScheduleSave();
  public void PurpleBlistRequestAddBuddy(int account, String username, String group, String alias);
  public void PurpleBlistRequestAddChat(int account, int group, String alias, String name);
  public void PurpleBlistRequestAddGroup();
  public void PurpleBlistNodeSetBool(int node, String key, int value);
  public int PurpleBlistNodeGetBool(int node, String key);
  public void PurpleBlistNodeSetInt(int node, String key, int value);
  public int PurpleBlistNodeGetInt(int node, String key);
  public void PurpleBlistNodeSetString(int node, String key, String value);
  public String PurpleBlistNodeGetString(int node, String key);
  public void PurpleBlistNodeRemoveSetting(int node, String key);
  public void PurpleBlistNodeSetFlags(int node, int flags);
  public int PurpleBlistNodeGetFlags(int node);
  public int PurpleBlistNodeGetType(int node);
  public List<Integer> PurpleBlistNodeGetExtendedMenu(int n);
  public void PurpleBlistSetUiOps(int ops);
  public int PurpleBlistGetUiOps();
  public void PurpleBlistInit();
  public void PurpleBlistUninit();
  public int PurpleBuddyIconNew(int account, String username, int icon_data, int icon_len, String checksum);
  public int PurpleBuddyIconRef(int icon);
  public int PurpleBuddyIconUnref(int icon);
  public void PurpleBuddyIconUpdate(int icon);
  public void PurpleBuddyIconSetData(int icon, int data, int len, String checksum);
  public int PurpleBuddyIconGetAccount(int icon);
  public String PurpleBuddyIconGetUsername(int icon);
  public String PurpleBuddyIconGetChecksum(int icon);
  public List<Byte> PurpleBuddyIconGetData(int icon);
  public String PurpleBuddyIconGetExtension(int icon);
  public String PurpleBuddyIconGetFullPath(int icon);
  public void PurpleBuddyIconsSetForUser(int account, String username, int icon_data, int icon_len, String checksum);
  public int PurpleBuddyIconsFind(int account, String username);
  public int PurpleBuddyIconsFindAccountIcon(int account);
  public int PurpleBuddyIconsSetAccountIcon(int account, int icon_data, int icon_len);
  public int PurpleBuddyIconsGetAccountIconTimestamp(int account);
  public int PurpleBuddyIconsNodeHasCustomIcon(int node);
  public int PurpleBuddyIconsNodeFindCustomIcon(int node);
  public int PurpleBuddyIconsNodeSetCustomIcon(int node, int icon_data, int icon_len);
  public int PurpleBuddyIconsNodeSetCustomIconFromFile(int node, String filename);
  public int PurpleBuddyIconsHasCustomIcon(int contact);
  public int PurpleBuddyIconsFindCustomIcon(int contact);
  public int PurpleBuddyIconsSetCustomIcon(int contact, int icon_data, int icon_len);
  public void PurpleBuddyIconsSetCaching(int caching);
  public int PurpleBuddyIconsIsCaching();
  public void PurpleBuddyIconsSetCacheDir(String cache_dir);
  public String PurpleBuddyIconsGetCacheDir();
  public void PurpleBuddyIconsInit();
  public void PurpleBuddyIconsUninit();
  public void PurpleBuddyIconGetScaleSize(int spec, int width, int height);
  public void PurpleConnectionNew(int account, int regist, String password);
  public void PurpleConnectionDestroy(int gc);
  public void PurpleConnectionSetState(int gc, int state);
  public void PurpleConnectionSetAccount(int gc, int account);
  public void PurpleConnectionSetDisplayName(int gc, String name);
  public void PurpleConnectionSetProtocolData(int connection, int proto_data);
  public int PurpleConnectionGetState(int gc);
  public int PurpleConnectionGetAccount(int gc);
  public int PurpleConnectionGetPrpl(int gc);
  public String PurpleConnectionGetPassword(int gc);
  public String PurpleConnectionGetDisplayName(int gc);
  public void PurpleConnectionUpdateProgress(int gc, String text, int step, int count);
  public void PurpleConnectionNotice(int gc, String text);
  public void PurpleConnectionError(int gc, String reason);
  public void PurpleConnectionErrorReason(int gc, int reason, String description);
  public void PurpleConnectionSslError(int gc, int ssl_error);
  public int PurpleConnectionErrorIsFatal(int reason);
  public void PurpleConnectionsDisconnectAll();
  public List<Integer> PurpleConnectionsGetAll();
  public List<Integer> PurpleConnectionsGetConnecting();
  public void PurpleConnectionsSetUiOps(int ops);
  public int PurpleConnectionsGetUiOps();
  public void PurpleConnectionsInit();
  public void PurpleConnectionsUninit();
  public int PurpleConversationNew(int type, int account, String name);
  public void PurpleConversationDestroy(int conv);
  public void PurpleConversationPresent(int conv);
  public int PurpleConversationGetType(int conv);
  public void PurpleConversationSetUiOps(int conv, int ops);
  public void PurpleConversationsSetUiOps(int ops);
  public int PurpleConversationGetUiOps(int conv);
  public void PurpleConversationSetAccount(int conv, int account);
  public int PurpleConversationGetAccount(int conv);
  public int PurpleConversationGetGc(int conv);
  public void PurpleConversationSetTitle(int conv, String title);
  public String PurpleConversationGetTitle(int conv);
  public void PurpleConversationAutosetTitle(int conv);
  public void PurpleConversationSetName(int conv, String name);
  public String PurpleConversationGetName(int conv);
  public void PurpleConversationSetLogging(int conv, int log);
  public int PurpleConversationIsLogging(int conv);
  public int PurpleConversationGetImData(int conv);
  public int PurpleConversationGetChatData(int conv);
  public List<Integer> PurpleGetConversations();
  public List<Integer> PurpleGetIms();
  public List<Integer> PurpleGetChats();
  public int PurpleFindConversationWithAccount(int type, String name, int account);
  public void PurpleConversationWrite(int conv, String who, String message, int flags, int mtime);
  public void PurpleConversationSetFeatures(int conv, int features);
  public int PurpleConversationGetFeatures(int conv);
  public int PurpleConversationHasFocus(int conv);
  public void PurpleConversationUpdate(int conv, int type);
  public List<Integer> PurpleConversationGetMessageHistory(int conv);
  public void PurpleConversationClearMessageHistory(int conv);
  public String PurpleConversationMessageGetSender(int msg);
  public String PurpleConversationMessageGetMessage(int msg);
  public int PurpleConversationMessageGetFlags(int msg);
  public int PurpleConversationMessageGetTimestamp(int msg);
  public int PurpleConvImGetConversation(int im);
  public void PurpleConvImSetIcon(int im, int icon);
  public int PurpleConvImGetIcon(int im);
  public void PurpleConvImSetTypingState(int im, int state);
  public int PurpleConvImGetTypingState(int im);
  public void PurpleConvImStartTypingTimeout(int im, int timeout);
  public void PurpleConvImStopTypingTimeout(int im);
  public int PurpleConvImGetTypingTimeout(int im);
  public void PurpleConvImSetTypeAgain(int im, UInt32 val);
  public int PurpleConvImGetTypeAgain(int im);
  public void PurpleConvImStartSendTypedTimeout(int im);
  public void PurpleConvImStopSendTypedTimeout(int im);
  public int PurpleConvImGetSendTypedTimeout(int im);
  public void PurpleConvImUpdateTyping(int im);
  public void PurpleConvImWrite(int im, String who, String message, int flags, int mtime);
  public int PurpleConvPresentError(String who, int account, String what);
  public void PurpleConvImSend(int im, String message);
  public void PurpleConvSendConfirm(int conv, String message);
  public void PurpleConvImSendWithFlags(int im, String message, int flags);
  public int PurpleConvCustomSmileyAdd(int conv, String smile, String cksum_type, String chksum, int remote);
  public void PurpleConvCustomSmileyClose(int conv, String smile);
  public int PurpleConvChatGetConversation(int chat);
  public List<Integer> PurpleConvChatSetUsers(int chat, int users);
  public List<Integer> PurpleConvChatGetUsers(int chat);
  public void PurpleConvChatIgnore(int chat, String name);
  public void PurpleConvChatUnignore(int chat, String name);
  public List<Integer> PurpleConvChatSetIgnored(int chat, int ignored);
  public List<Integer> PurpleConvChatGetIgnored(int chat);
  public String PurpleConvChatGetIgnoredUser(int chat, String user);
  public int PurpleConvChatIsUserIgnored(int chat, String user);
  public void PurpleConvChatSetTopic(int chat, String who, String topic);
  public String PurpleConvChatGetTopic(int chat);
  public void PurpleConvChatSetId(int chat, int id);
  public int PurpleConvChatGetId(int chat);
  public void PurpleConvChatWrite(int chat, String who, String message, int flags, int mtime);
  public void PurpleConvChatSend(int chat, String message);
  public void PurpleConvChatSendWithFlags(int chat, String message, int flags);
  public void PurpleConvChatAddUser(int chat, String user, String extra_msg, int flags, int new_arrival);
  public void PurpleConvChatAddUsers(int chat, int users, int extra_msgs, int flags, int new_arrivals);
  public void PurpleConvChatRenameUser(int chat, String old_user, String new_user);
  public void PurpleConvChatRemoveUser(int chat, String user, String reason);
  public void PurpleConvChatRemoveUsers(int chat, int users, String reason);
  public int PurpleConvChatFindUser(int chat, String user);
  public void PurpleConvChatUserSetFlags(int chat, String user, int flags);
  public int PurpleConvChatUserGetFlags(int chat, String user);
  public void PurpleConvChatClearUsers(int chat);
  public void PurpleConvChatSetNick(int chat, String nick);
  public String PurpleConvChatGetNick(int chat);
  public int PurpleFindChat(int gc, int id);
  public void PurpleConvChatLeft(int chat);
  public void PurpleConvChatInviteUser(int chat, String user, String message, int confirm);
  public int PurpleConvChatHasLeft(int chat);
  public int PurpleConvChatCbNew(String name, String alias, int flags);
  public int PurpleConvChatCbFind(int chat, String name);
  public String PurpleConvChatCbGetName(int cb);
  public void PurpleConvChatCbDestroy(int cb);
  public List<Integer> PurpleConversationGetExtendedMenu(int conv);
  public void PurpleConversationsInit();
  public void PurpleConversationsUninit();
  public int PurpleCoreInit(String ui);
  public void PurpleCoreQuit();
  public String PurpleCoreGetVersion();
  public String PurpleCoreGetUi();
  public int PurpleGetCore();
  public void PurpleCoreSetUiOps(int ops);
  public int PurpleCoreGetUiOps();
  public int PurpleCoreMigrate();
  public int PurpleCoreEnsureSingleInstance();
  public int PurpleXferNew(int account, int type, String who);
  public List<Integer> PurpleXfersGetAll();
  public void PurpleXferRef(int xfer);
  public void PurpleXferUnref(int xfer);
  public void PurpleXferRequest(int xfer);
  public void PurpleXferRequestAccepted(int xfer, String filename);
  public void PurpleXferRequestDenied(int xfer);
  public int PurpleXferGetType(int xfer);
  public int PurpleXferGetAccount(int xfer);
  public String PurpleXferGetRemoteUser(int xfer);
  public int PurpleXferGetStatus(int xfer);
  public int PurpleXferIsCanceled(int xfer);
  public int PurpleXferIsCompleted(int xfer);
  public String PurpleXferGetFilename(int xfer);
  public String PurpleXferGetLocalFilename(int xfer);
  public int PurpleXferGetBytesSent(int xfer);
  public int PurpleXferGetBytesRemaining(int xfer);
  public int PurpleXferGetSize(int xfer);
  public UInt32 PurpleXferGetLocalPort(int xfer);
  public String PurpleXferGetRemoteIp(int xfer);
  public UInt32 PurpleXferGetRemotePort(int xfer);
  public int PurpleXferGetStartTime(int xfer);
  public int PurpleXferGetEndTime(int xfer);
  public void PurpleXferSetCompleted(int xfer, int completed);
  public void PurpleXferSetMessage(int xfer, String message);
  public void PurpleXferSetFilename(int xfer, String filename);
  public void PurpleXferSetLocalFilename(int xfer, String filename);
  public void PurpleXferSetSize(int xfer, int size);
  public void PurpleXferSetBytesSent(int xfer, int bytes_sent);
  public int PurpleXferGetUiOps(int xfer);
  public void PurpleXferStart(int xfer, int fd, String ip, UInt32 port);
  public void PurpleXferEnd(int xfer);
  public void PurpleXferAdd(int xfer);
  public void PurpleXferCancelLocal(int xfer);
  public void PurpleXferCancelRemote(int xfer);
  public void PurpleXferError(int type, int account, String who, String msg);
  public void PurpleXferUpdateProgress(int xfer);
  public List<Byte> PurpleXferGetThumbnail(int xfer);
  public String PurpleXferGetThumbnailMimetype(int xfer);
  public void PurpleXferPrepareThumbnail(int xfer, String formats);
  public void PurpleXfersInit();
  public void PurpleXfersUninit();
  public void PurpleXfersSetUiOps(int ops);
  public int PurpleXfersGetUiOps();
  public void PurpleLogFree(int log);
  public void PurpleLogWrite(int log, int type, String from, int time, String message);
  public List<Integer> PurpleLogGetLogs(int type, String name, int account);
  public List<Integer> PurpleLogGetSystemLogs(int account);
  public int PurpleLogGetSize(int log);
  public int PurpleLogGetTotalSize(int type, String name, int account);
  public int PurpleLogGetActivityScore(int type, String name, int account);
  public int PurpleLogIsDeletable(int log);
  public int PurpleLogDelete(int log);
  public String PurpleLogGetLogDir(int type, String name, int account);
  public void PurpleLogSetFree(int set);
  public void PurpleLogCommonWriter(int log, String ext);
  public List<Integer> PurpleLogCommonLister(int type, String name, int account, String ext, int logger);
  public int PurpleLogCommonTotalSizer(int type, String name, int account, String ext);
  public int PurpleLogCommonSizer(int log);
  public int PurpleLogCommonDeleter(int log);
  public int PurpleLogCommonIsDeletable(int log);
  public void PurpleLogLoggerFree(int logger);
  public void PurpleLogLoggerAdd(int logger);
  public void PurpleLogLoggerRemove(int logger);
  public void PurpleLogLoggerSet(int logger);
  public int PurpleLogLoggerGet();
  public List<Integer> PurpleLogLoggerGetOptions();
  public void PurpleLogInit();
  public void PurpleLogUninit();
  public void PurpleNotifySearchresultsFree(int results);
  public void PurpleNotifySearchresultsNewRows(int gc, int results, int data);
  public int PurpleNotifySearchresultsNew();
  public int PurpleNotifySearchresultsColumnNew(String title);
  public void PurpleNotifySearchresultsColumnAdd(int results, int column);
  public void PurpleNotifySearchresultsRowAdd(int results, int row);
  public int PurpleNotifySearchresultsGetRowsCount(int results);
  public int PurpleNotifySearchresultsGetColumnsCount(int results);
  public List<Integer> PurpleNotifySearchresultsRowGet(int results, UInt32 row_id);
  public String PurpleNotifySearchresultsColumnGetTitle(int results, UInt32 column_id);
  public int PurpleNotifyUserInfoNew();
  public void PurpleNotifyUserInfoDestroy(int user_info);
  public List<Integer> PurpleNotifyUserInfoGetEntries(int user_info);
  public String PurpleNotifyUserInfoGetTextWithNewline(int user_info, String newline);
  public void PurpleNotifyUserInfoAddPair(int user_info, String label, String value);
  public void PurpleNotifyUserInfoPrependPair(int user_info, String label, String value);
  public void PurpleNotifyUserInfoRemoveEntry(int user_info, int user_info_entry);
  public int PurpleNotifyUserInfoEntryNew(String label, String value);
  public void PurpleNotifyUserInfoAddSectionBreak(int user_info);
  public void PurpleNotifyUserInfoPrependSectionBreak(int user_info);
  public void PurpleNotifyUserInfoAddSectionHeader(int user_info, String label);
  public void PurpleNotifyUserInfoPrependSectionHeader(int user_info, String label);
  public void PurpleNotifyUserInfoRemoveLastItem(int user_info);
  public String PurpleNotifyUserInfoEntryGetLabel(int user_info_entry);
  public void PurpleNotifyUserInfoEntrySetLabel(int user_info_entry, String label);
  public String PurpleNotifyUserInfoEntryGetValue(int user_info_entry);
  public void PurpleNotifyUserInfoEntrySetValue(int user_info_entry, String value);
  public int PurpleNotifyUserInfoEntryGetType(int user_info_entry);
  public void PurpleNotifyUserInfoEntrySetType(int user_info_entry, int type);
  public void PurpleNotifyClose(int type, int ui_handle);
  public void PurpleNotifyCloseWithHandle(int handle);
  public void PurpleNotifySetUiOps(int ops);
  public int PurpleNotifyGetUiOps();
  public void PurpleNotifyInit();
  public void PurpleNotifyUninit();
  public void PurplePrefsInit();
  public void PurplePrefsUninit();
  public void PurplePrefsAddNone(String name);
  public void PurplePrefsAddBool(String name, int value);
  public void PurplePrefsAddInt(String name, int value);
  public void PurplePrefsAddString(String name, String value);
  public void PurplePrefsAddStringList(String name, int value);
  public void PurplePrefsAddPath(String name, String value);
  public void PurplePrefsAddPathList(String name, int value);
  public void PurplePrefsRemove(String name);
  public void PurplePrefsRename(String oldname, String newname);
  public void PurplePrefsRenameBooleanToggle(String oldname, String newname);
  public void PurplePrefsDestroy();
  public void PurplePrefsSetBool(String name, int value);
  public void PurplePrefsSetInt(String name, int value);
  public void PurplePrefsSetString(String name, String value);
  public void PurplePrefsSetStringList(String name, int value);
  public void PurplePrefsSetPath(String name, String value);
  public void PurplePrefsSetPathList(String name, int value);
  public int PurplePrefsExists(String name);
  public int PurplePrefsGetType(String name);
  public int PurplePrefsGetBool(String name);
  public int PurplePrefsGetInt(String name);
  public String PurplePrefsGetString(String name);
  public List<String> PurplePrefsGetStringList(String name);
  public String PurplePrefsGetPath(String name);
  public List<String> PurplePrefsGetPathList(String name);
  public List<String> PurplePrefsGetChildrenNames(String name);
  public void PurplePrefsDisconnectCallback(int callback_id);
  public void PurplePrefsDisconnectByHandle(int handle);
  public void PurplePrefsTriggerCallback(String name);
  public int PurplePrefsLoad();
  public void PurplePrefsUpdateOld();
  public void PurpleRoomlistShowWithAccount(int account);
  public int PurpleRoomlistNew(int account);
  public void PurpleRoomlistRef(int list);
  public void PurpleRoomlistUnref(int list);
  public void PurpleRoomlistSetFields(int list, int fields);
  public void PurpleRoomlistSetInProgress(int list, int in_progress);
  public int PurpleRoomlistGetInProgress(int list);
  public void PurpleRoomlistRoomAdd(int list, int room);
  public int PurpleRoomlistGetList(int gc);
  public void PurpleRoomlistCancelGetList(int list);
  public void PurpleRoomlistExpandCategory(int list, int category);
  public List<Integer> PurpleRoomlistGetFields(int roomlist);
  public int PurpleRoomlistRoomNew(int type, String name, int parent);
  public void PurpleRoomlistRoomJoin(int list, int room);
  public int PurpleRoomlistRoomGetType(int room);
  public String PurpleRoomlistRoomGetName(int room);
  public int PurpleRoomlistRoomGetParent(int room);
  public List<Integer> PurpleRoomlistRoomGetFields(int room);
  public int PurpleRoomlistFieldNew(int type, String label, String name, int hidden);
  public int PurpleRoomlistFieldGetType(int field);
  public String PurpleRoomlistFieldGetLabel(int field);
  public int PurpleRoomlistFieldGetHidden(int field);
  public void PurpleRoomlistSetUiOps(int ops);
  public int PurpleRoomlistGetUiOps();
  public int PurpleSavedstatusNew(String title, int type);
  public void PurpleSavedstatusSetTitle(int status, String title);
  public void PurpleSavedstatusSetType(int status, int type);
  public void PurpleSavedstatusSetMessage(int status, String message);
  public void PurpleSavedstatusSetSubstatus(int status, int account, int type, String message);
  public void PurpleSavedstatusUnsetSubstatus(int saved_status, int account);
  public int PurpleSavedstatusDelete(String title);
  public void PurpleSavedstatusDeleteByStatus(int saved_status);
  public List<Integer> PurpleSavedstatusesGetAll();
  public List<Integer> PurpleSavedstatusesGetPopular(UInt32 how_many);
  public int PurpleSavedstatusGetCurrent();
  public int PurpleSavedstatusGetDefault();
  public int PurpleSavedstatusGetIdleaway();
  public int PurpleSavedstatusIsIdleaway();
  public void PurpleSavedstatusSetIdleaway(int idleaway);
  public int PurpleSavedstatusGetStartup();
  public int PurpleSavedstatusFind(String title);
  public int PurpleSavedstatusFindByCreationTime(int creation_time);
  public int PurpleSavedstatusFindTransientByTypeAndMessage(int type, String message);
  public int PurpleSavedstatusIsTransient(int saved_status);
  public String PurpleSavedstatusGetTitle(int saved_status);
  public int PurpleSavedstatusGetType(int saved_status);
  public String PurpleSavedstatusGetMessage(int saved_status);
  public int PurpleSavedstatusGetCreationTime(int saved_status);
  public int PurpleSavedstatusHasSubstatuses(int saved_status);
  public int PurpleSavedstatusGetSubstatus(int saved_status, int account);
  public int PurpleSavedstatusSubstatusGetType(int substatus);
  public String PurpleSavedstatusSubstatusGetMessage(int substatus);
  public void PurpleSavedstatusActivate(int saved_status);
  public void PurpleSavedstatusActivateForAccount(int saved_status, int account);
  public void PurpleSavedstatusesInit();
  public void PurpleSavedstatusesUninit();
  public int PurpleSmileyNew(int img, String shortcut);
  public int PurpleSmileyNewFromFile(String shortcut, String filepath);
  public void PurpleSmileyDelete(int smiley);
  public int PurpleSmileySetShortcut(int smiley, String shortcut);
  public void PurpleSmileySetData(int smiley, int smiley_data, int smiley_data_len);
  public String PurpleSmileyGetShortcut(int smiley);
  public String PurpleSmileyGetChecksum(int smiley);
  public int PurpleSmileyGetStoredImage(int smiley);
  public List<Byte> PurpleSmileyGetData(int smiley);
  public String PurpleSmileyGetExtension(int smiley);
  public String PurpleSmileyGetFullPath(int smiley);
  public List<Integer> PurpleSmileysGetAll();
  public int PurpleSmileysFindByShortcut(String shortcut);
  public int PurpleSmileysFindByChecksum(String checksum);
  public String PurpleSmileysGetStoringDir();
  public void PurpleSmileysInit();
  public void PurpleSmileysUninit();
  public String PurplePrimitiveGetIdFromType(int type);
  public String PurplePrimitiveGetNameFromType(int type);
  public int PurplePrimitiveGetTypeFromId(String id);
  public int PurpleStatusTypeNewFull(int primitive, String id, String name, int saveable, int user_settable, int independent);
  public int PurpleStatusTypeNew(int primitive, String id, String name, int user_settable);
  public void PurpleStatusTypeDestroy(int status_type);
  public void PurpleStatusTypeSetPrimaryAttr(int status_type, String attr_id);
  public void PurpleStatusTypeAddAttr(int status_type, String id, String name, int value);
  public int PurpleStatusTypeGetPrimitive(int status_type);
  public String PurpleStatusTypeGetId(int status_type);
  public String PurpleStatusTypeGetName(int status_type);
  public int PurpleStatusTypeIsSaveable(int status_type);
  public int PurpleStatusTypeIsUserSettable(int status_type);
  public int PurpleStatusTypeIsIndependent(int status_type);
  public int PurpleStatusTypeIsExclusive(int status_type);
  public int PurpleStatusTypeIsAvailable(int status_type);
  public String PurpleStatusTypeGetPrimaryAttr(int type);
  public int PurpleStatusTypeGetAttr(int status_type, String id);
  public List<Integer> PurpleStatusTypeGetAttrs(int status_type);
  public int PurpleStatusTypeFindWithId(int status_types, String id);
  public int PurpleStatusAttrNew(String id, String name, int value_type);
  public void PurpleStatusAttrDestroy(int attr);
  public String PurpleStatusAttrGetId(int attr);
  public String PurpleStatusAttrGetName(int attr);
  public int PurpleStatusAttrGetValue(int attr);
  public int PurpleStatusNew(int status_type, int presence);
  public void PurpleStatusDestroy(int status);
  public void PurpleStatusSetActive(int status, int active);
  public void PurpleStatusSetActiveWithAttrsList(int status, int active, int attrs);
  public void PurpleStatusSetAttrBoolean(int status, String id, int value);
  public void PurpleStatusSetAttrInt(int status, String id, int value);
  public void PurpleStatusSetAttrString(int status, String id, String value);
  public int PurpleStatusGetType(int status);
  public int PurpleStatusGetPresence(int status);
  public String PurpleStatusGetId(int status);
  public String PurpleStatusGetName(int status);
  public int PurpleStatusIsIndependent(int status);
  public int PurpleStatusIsExclusive(int status);
  public int PurpleStatusIsAvailable(int status);
  public int PurpleStatusIsActive(int status);
  public int PurpleStatusIsOnline(int status);
  public int PurpleStatusGetAttrValue(int status, String id);
  public int PurpleStatusGetAttrBoolean(int status, String id);
  public int PurpleStatusGetAttrInt(int status, String id);
  public String PurpleStatusGetAttrString(int status, String id);
  public int PurpleStatusCompare(int status1, int status2);
  public int PurplePresenceNew(int context);
  public int PurplePresenceNewForAccount(int account);
  public int PurplePresenceNewForConv(int conv);
  public int PurplePresenceNewForBuddy(int buddy);
  public void PurplePresenceDestroy(int presence);
  public void PurplePresenceAddStatus(int presence, int status);
  public void PurplePresenceSetStatusActive(int presence, String status_id, int active);
  public void PurplePresenceSwitchStatus(int presence, String status_id);
  public void PurplePresenceSetIdle(int presence, int idle, int idle_time);
  public void PurplePresenceSetLoginTime(int presence, int login_time);
  public int PurplePresenceGetContext(int presence);
  public int PurplePresenceGetAccount(int presence);
  public int PurplePresenceGetConversation(int presence);
  public String PurplePresenceGetChatUser(int presence);
  public int PurplePresenceGetBuddy(int presence);
  public List<Integer> PurplePresenceGetStatuses(int presence);
  public int PurplePresenceGetStatus(int presence, String status_id);
  public int PurplePresenceGetActiveStatus(int presence);
  public int PurplePresenceIsAvailable(int presence);
  public int PurplePresenceIsOnline(int presence);
  public int PurplePresenceIsStatusActive(int presence, String status_id);
  public int PurplePresenceIsStatusPrimitiveActive(int presence, int primitive);
  public int PurplePresenceIsIdle(int presence);
  public int PurplePresenceGetIdleTime(int presence);
  public int PurplePresenceGetLoginTime(int presence);
  public int PurplePresenceCompare(int presence1, int presence2);
  public void PurpleStatusInit();
  public void PurpleStatusUninit();
  public UInt32 ServSendTyping(int gc, String name, int state);
  public void ServMoveBuddy(int param0, int param1, int param2);
  public int ServSendIm(int param0, String param1, String param2, int flags);
  public int PurpleGetAttentionTypeFromCode(int account, int type_code);
  public void ServSendAttention(int gc, String who, int type_code);
  public void ServGotAttention(int gc, String who, int type_code);
  public void ServGetInfo(int param0, String param1);
  public void ServSetInfo(int param0, String param1);
  public void ServAddPermit(int param0, String param1);
  public void ServAddDeny(int param0, String param1);
  public void ServRemPermit(int param0, String param1);
  public void ServRemDeny(int param0, String param1);
  public void ServSetPermitDeny(int param0);
  public void ServChatInvite(int param0, int param1, String param2, String param3);
  public void ServChatLeave(int param0, int param1);
  public void ServChatWhisper(int param0, int param1, String param2, String param3);
  public int ServChatSend(int param0, int param1, String param2, int flags);
  public void ServAliasBuddy(int param0);
  public void ServGotAlias(int gc, String who, String alias);
  public void PurpleServGotPrivateAlias(int gc, String who, String alias);
  public void ServGotTyping(int gc, String name, int timeout, int state);
  public void ServGotTypingStopped(int gc, String name);
  public void ServGotIm(int gc, String who, String msg, int flags, int mtime);
  public void ServJoinChat(int param0, Map<String,String> data);
  public void ServRejectChat(int param0, Map<String,String> data);
  public void ServGotChatInvite(int gc, String name, String who, String message, Map<String,String> data);
  public int ServGotJoinedChat(int gc, int id, String name);
  public void PurpleServGotJoinChatFailed(int gc, Map<String,String> data);
  public void ServGotChatLeft(int g, int id);
  public void ServGotChatIn(int g, int id, String who, int flags, String message, int mtime);
  public void ServSendFile(int gc, String who, String file);
  public void PurpleMenuActionFree(int act);
  public void PurpleUtilSetCurrentSong(String title, String artist, String album);
  public void PurpleUtilInit();
  public void PurpleUtilUninit();
  public String PurpleMimeDecodeField(String str);
  public int PurpleTimeBuild(int year, int month, int day, int hour, int min, int sec);
  public String PurpleMarkupEscapeText(String text, int length);
  public String PurpleMarkupStripHtml(String str);
  public String PurpleMarkupLinkify(String str);
  public String PurpleUnescapeText(String text);
  public String PurpleUnescapeHtml(String html);
  public String PurpleMarkupSlice(String str, int x, int y);
  public String PurpleMarkupGetTagName(String tag);
  public String PurpleMarkupUnescapeEntity(String text, int length);
  public String PurpleMarkupGetCssProperty(String style, String opt);
  public int PurpleMarkupIsRtl(String html);
  public String PurpleHomeDir();
  public String PurpleUserDir();
  public void PurpleUtilSetUserDir(String dir);
  public int PurpleBuildDir(String path, int mode);
  public int PurpleUtilWriteDataToFile(String filename, String data, int size);
  public int PurpleUtilWriteDataToFileAbsolute(String filename_full, String data, int size);
  public int PurpleProgramIsValid(String program);
  public int PurpleRunningGnome();
  public int PurpleRunningKde();
  public int PurpleRunningOsx();
  public String PurpleFdGetIp(int fd);
  public int PurpleSocketGetFamily(int fd);
  public int PurpleSocketSpeaksIpv4(int fd);
  public int PurpleStrequal(String left, String right);
  public String PurpleNormalize(int account, String str);
  public String PurpleNormalizeNocase(int account, String str);
  public int PurpleStrHasPrefix(String s, String p);
  public int PurpleStrHasSuffix(String s, String x);
  public String PurpleStrdupWithhtml(String src);
  public String PurpleStrAddCr(String str);
  public String PurpleStrreplace(String string, String delimiter, String replacement);
  public String PurpleUtf8NcrEncode(String in);
  public String PurpleUtf8NcrDecode(String in);
  public String PurpleStrcasereplace(String string, String delimiter, String replacement);
  public String PurpleStrcasestr(String haystack, String needle);
  public String PurpleStrSizeToUnits(int size);
  public String PurpleStrSecondsToString(int sec);
  public String PurpleStrBinaryToAscii(String binary, int len);
  public void PurpleGotProtocolHandlerUri(String uri);
  public void PurpleUtilFetchUrlCancel(int url_data);
  public String PurpleUrlDecode(String str);
  public String PurpleUrlEncode(String str);
  public int PurpleEmailIsValid(String address);
  public int PurpleIpAddressIsValid(String ip);
  public int PurpleIpv4AddressIsValid(String ip);
  public int PurpleIpv6AddressIsValid(String ip);
  public List<String> PurpleUriListExtractUris(String uri_list);
  public List<String> PurpleUriListExtractFilenames(String uri_list);
  public String PurpleUtf8TryConvert(String str);
  public String PurpleUtf8Salvage(String str);
  public String PurpleUtf8StripUnprintables(String str);
  public int PurpleUtf8Strcasecmp(String a, String b);
  public int PurpleUtf8HasWord(String haystack, String needle);
  public String PurpleTextStripMnemonic(String in);
  public String PurpleUnescapeFilename(String str);
  public String PurpleEscapeFilename(String str);
  public String PurpleOscarConvert(String act, String protocol);
  public void PurpleRestoreDefaultSignalHandlers();
  public String PurpleGetHostName();
  public String PurpleUuidRandom();
  public void XmlnodeInsertChild(int parent, int child);
  public void XmlnodeInsertData(int node, String data, int size);
  public String XmlnodeGetData(int node);
  public String XmlnodeGetDataUnescaped(int node);
  public void XmlnodeSetAttrib(int node, String attr, String value);
  public void XmlnodeSetAttribWithPrefix(int node, String attr, String prefix, String value);
  public void XmlnodeSetAttribWithNamespace(int node, String attr, String xmlns, String value);
  public void XmlnodeSetAttribFull(int node, String attr, String xmlns, String prefix, String value);
  public String XmlnodeGetAttrib(int node, String attr);
  public String XmlnodeGetAttribWithNamespace(int node, String attr, String xmlns);
  public void XmlnodeRemoveAttrib(int node, String attr);
  public void XmlnodeRemoveAttribWithNamespace(int node, String attr, String xmlns);
  public void XmlnodeSetNamespace(int node, String xmlns);
  public String XmlnodeGetNamespace(int node);
  public void XmlnodeSetPrefix(int node, String prefix);
  public String XmlnodeGetPrefix(int node);
  public String XmlnodeToStr(int node, int len);
  public String XmlnodeToFormattedStr(int node, int len);
  public void XmlnodeFree(int node);
  public int PurpleAttentionTypeNew(String ulname, String name, String inc_desc, String out_desc);
  public void PurpleAttentionTypeSetName(int type, String name);
  public void PurpleAttentionTypeSetIncomingDesc(int type, String desc);
  public void PurpleAttentionTypeSetOutgoingDesc(int type, String desc);
  public void PurpleAttentionTypeSetIconName(int type, String name);
  public void PurpleAttentionTypeSetUnlocalizedName(int type, String ulname);
  public String PurpleAttentionTypeGetName(int type);
  public String PurpleAttentionTypeGetIncomingDesc(int type);
  public String PurpleAttentionTypeGetOutgoingDesc(int type);
  public String PurpleAttentionTypeGetIconName(int type);
  public String PurpleAttentionTypeGetUnlocalizedName(int type);
  public void PurplePrplGotAccountIdle(int account, int idle, int idle_time);
  public void PurplePrplGotAccountLoginTime(int account, int login_time);
  public void PurplePrplGotAccountActions(int account);
  public void PurplePrplGotUserIdle(int account, String name, int idle, int idle_time);
  public void PurplePrplGotUserLoginTime(int account, String name, int login_time);
  public void PurplePrplGotUserStatusDeactive(int account, String name, String status_id);
  public void PurplePrplChangeAccountStatus(int account, int old_status, int new_status);
  public List<Integer> PurplePrplGetStatuses(int account, int presence);
  public void PurplePrplSendAttention(int gc, String who, int type_code);
  public void PurplePrplGotAttention(int gc, String who, int type_code);
  public void PurplePrplGotAttentionInChat(int gc, int id, String who, int type_code);
  public int PurplePrplGetMediaCaps(int account, String who);
  public int PurplePrplInitiateMedia(int account, String who, int type);
  public void PurplePrplGotMediaCaps(int account, String who);
  public int PurpleFindPrpl(String id);

}
