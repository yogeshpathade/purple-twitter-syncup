/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.dbus;

import im.pidgin.purple.PurpleInterface;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PurpleDBusConnection {
    private DBusConnection bus = null;
    Logger logger = LoggerFactory.getLogger(PurpleDBusConnection.class);
    
    public PurpleDBusConnection(){    	
    	init();
    }
    
    private void init(){
        try {
			bus = DBusConnection.getConnection(DBusConnection.SESSION);
			logger.info("DBus SESSION connection is initialized.");
		} catch (DBusException e) {
			logger.error("Error getting DBus Session connection.");
			e.printStackTrace();
		}
    }
    
    public PurpleInterface getPurpleObject(){
        PurpleInterface purple = null;
    	if(bus == null){ 	
    		logger.error("DBus is not initialized.");
    	}
    	
		try {
			purple = (PurpleInterface)bus.getRemoteObject(
					DBusSyncupConstants.PURPLE_SERVICE, DBusSyncupConstants.PURPLE_OBJ);
			
		} catch (DBusException e) {
			e.printStackTrace();
		}
		
		return purple;
    }
}
