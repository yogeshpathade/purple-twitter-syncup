/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.dbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import im.pidgin.purple.PurpleInterface;

public class PurpleStatusManager {
	private PurpleInterface purple = null;
    private Logger logger = LoggerFactory.getLogger(PurpleStatusManager.class);

	public PurpleStatusManager(PurpleInterface purple){
		this.purple = purple;
	}
	
	public boolean setPurpleStatus(String statusMsg){
		boolean sflag = true;
		logger.info("set Purple Status message to : " + statusMsg);
		try{
			int iStatus = purple.PurpleSavedstatusGetType(purple.PurpleSavedstatusGetCurrent());
			logger.info("Purple status is : "+ iStatus);
			int status = purple.PurpleSavedstatusNew("", iStatus);
			logger.info("Purple status will be changed to : " + status);
		    purple.PurpleSavedstatusSetMessage((int)status, statusMsg);
		    purple.PurpleSavedstatusActivate(status);
		}catch(Exception ex){
			logger.error("Something is wrong with DBus connection with Purple");
			sflag = false;
		}	    
		return sflag;
	}
}
