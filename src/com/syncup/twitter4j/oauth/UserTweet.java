/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.twitter4j.oauth;

import java.util.List;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class UserTweet {
	private Twitter twitter = null;
	
	public UserTweet(Twitter twitter){
		this.twitter = twitter;
	}
	
	public List<Status> getUserTimeline(){
        List<Status> statuses = null;
        String user = null;
		try {
			user = twitter.verifyCredentials().getScreenName();
		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
        try {
			statuses = twitter.getUserTimeline();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return statuses;
	}
	
}
