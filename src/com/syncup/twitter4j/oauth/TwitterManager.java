/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.twitter4j.oauth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.internal.http.HttpClientImpl;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpRequest;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.http.RequestMethod;

public class TwitterManager {

	private Twitter twitter = null;
	private Properties props = null;
	
    private static final String TWITTER_CONFIG = "twitterConfig";
    private Logger logger = LoggerFactory.getLogger(TwitterManager.class);

	public TwitterManager() {
		initTwitConfig();
		twitter = new TwitterFactory().getInstance();
		initTwitterOAuth();
	}
	
	
    private void initTwitConfig(){
    	props = new Properties();
    	String propfile = System.getProperty(TWITTER_CONFIG);
    	logger.info("Twitter config file - " + propfile);
        loadProperties(props, propfile);
    }
    
    private void initTwitterOAuth(){
        HttpClientImpl http;
        HttpResponse response = null;
        String resStr = null;
        String authorizeURL;
        HttpParameter[] params;
        AccessToken at;
        String cookie;
        http = new HttpClientImpl();
        String user = props.getProperty("oauth.user");
        String passwd = props.getProperty("oauth.password");
		
        logger.info("OAuth Twitter Authentication started.");
        
		twitter.setOAuthConsumer(props.getProperty("oauth.consumerKey"), 
				props.getProperty("oauth.consumerSecret"));
		RequestToken rt = null;
		try {
			rt = twitter.getOAuthRequestToken(null, "read");
		} catch (TwitterException e) {
			logger.error("Error in getting Request Token", e);
			//e.printStackTrace();
		}
		
		logger.info("sending Authorization url request with the access token.");
        Map<String, String> props = new HashMap<String, String>();
        try {
			response = http.get(rt.getAuthorizationURL());
		} catch (TwitterException e) {
			logger.error("Error in getting response from Authorization url", e);
			//e.printStackTrace();
		}
		
		logger.info("Parsing response from Authorization url");
        cookie = response.getResponseHeader("Set-Cookie");
        props.put("Cookie", cookie);
        try {
			resStr = response.asString();
		} catch (TwitterException e) {
			logger.error("Error while parsing response from authorization url",e);
		}
		
		logger.info("Prearing for the HTTP Post request for sending user " +
				"authentication with Twitter to generate the pin.");
        authorizeURL = catchPattern(resStr, "<form action=\"", "\" id=\"oauth_form\"");
        params = new HttpParameter[4];
        params[0] = new HttpParameter("authenticity_token"
                , catchPattern(resStr, "\"authenticity_token\" type=\"hidden\" value=\"", "\" />"));
        params[1] = new HttpParameter("oauth_token",
                catchPattern(resStr, "name=\"oauth_token\" type=\"hidden\" value=\"", "\" />"));
        params[2] = new HttpParameter("session[username_or_email]", user);
        params[3] = new HttpParameter("session[password]", passwd);
        try {
			response = http.request(new HttpRequest(RequestMethod.POST, authorizeURL, params, null, props));
		} catch (TwitterException e) {
			logger.error("Error while authentication user with Twitter", e);
			//e.printStackTrace();
		}
        
		logger.info("Parsing response to get the user pin");
        try {
			resStr = response.asString();
		} catch (TwitterException e) {
			logger.error("Error while parsing HTTP Post response for pin", e);
		}
        String pin = catchPattern(resStr, "<kbd aria-labelledby=\"code-desc\"><code>", "</code></kbd>");
        
        logger.info("Sending request to get the Access Token");
        try {
			at = twitter.getOAuthAccessToken(rt, pin);
		} catch (TwitterException e) {
			logger.error("Error while getting the Access Token from Twitter.");
		}
    }
    
    
    
    public Twitter getTwitter() {
		return twitter;
	}


	private static String catchPattern(String body, String before, String after) {
        int beforeIndex = body.indexOf(before);
        int afterIndex = body.indexOf(after, beforeIndex);
        return body.substring(beforeIndex + before.length(), afterIndex);
    }
    
    private boolean loadProperties(Properties props, String path) {
        FileInputStream fis = null;
        try {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                fis = new FileInputStream(file);
                props.load(fis);
                return true;
            }
        }catch(FileNotFoundException ex){
        	logger.error("Twitter config file not found",ex);
        }catch (Exception ignore) {
        } finally {
            try {
                if (null != fis) {
                    fis.close();
                }
            } catch (IOException ignore) {
            }
        }
        return false;
    }
    
    public static void main(String[] argv){
    	TwitterManager twit = new TwitterManager();
    	
    }
	
	
}
