/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.TwitterException;
import im.pidgin.purple.PurpleInterface;

import com.syncup.dbus.PurpleDBusConnection;
import com.syncup.dbus.PurpleStatusManager;
import com.syncup.twitter4j.oauth.TwitterManager;
import com.syncup.twitter4j.oauth.UserTweet;

public class TwitterPurpleSyncup {
	private PurpleDBusConnection purpleConn = null;
	private PurpleInterface purple = null;
	private TwitterManager twitterMgr = null;
	
	private static Logger logger = LoggerFactory.getLogger(TwitterPurpleSyncup.class);
	
	public TwitterPurpleSyncup(){
		purpleConn = new PurpleDBusConnection();
		purple = purpleConn.getPurpleObject();
		twitterMgr = new TwitterManager();
	}
	
	public PurpleInterface getPurple() {
		return purple;
	}

	public TwitterManager getTwitterMgr() {
		return twitterMgr;
	}
	
	public String getUserScreenNm(){
		String user = null;
		try {
			user = twitterMgr.getTwitter().verifyCredentials().getScreenName();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
	
	public static void main(String[] args){
		TwitterPurpleSyncup sync = new TwitterPurpleSyncup();
		TwitterPurpleSyncupWorker worker = new TwitterPurpleSyncupWorker(
				new PurpleStatusManager(sync.getPurple()), 
				new UserTweet(sync.getTwitterMgr().getTwitter()), sync.getUserScreenNm());
		
		Thread updator = new Thread(worker);
		updator.start();
		
		logger.debug("Syncup application is running successfully.");
	}
}
