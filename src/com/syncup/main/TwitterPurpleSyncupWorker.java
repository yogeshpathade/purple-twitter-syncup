/*
 * This file is part of Purple Twitter Syncup Application.
 * Copyright 2012 Yogesh Pathade  
 *   
 * Purple Twitter Syncup is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Purple Twitter Syncup is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Purple Twitter Syncup. If not, see <http://www.gnu.org/licenses/>.
 */

package com.syncup.main;

import java.util.List;

import twitter4j.Status;

import com.syncup.dbus.PurpleStatusManager;
import com.syncup.twitter4j.oauth.UserTweet;


public class TwitterPurpleSyncupWorker implements Runnable {
	private UserTweet usrTweet = null;
	private PurpleStatusManager purpleSts = null;
	private String user = null;
	private boolean stopApp = false;
	private long id ;
	

	public TwitterPurpleSyncupWorker(PurpleStatusManager purpleSts,
			UserTweet usrTweet, String user) {
		this.purpleSts = purpleSts;
		this.usrTweet = usrTweet;
		this.user = user;
	}


	@Override
	public void run() {
		while(!stopApp){
			
			String tweet = getLatestTweetofUser();
			if(tweet!=null){
				purpleSts.setPurpleStatus(tweet);
			}
			
			try {
				Thread.sleep(30000);
				//TODO 	1. need to add capability to query rate limit
				//OR 	2. Make it configurable.				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


	private String getLatestTweetofUser() {
		String tweet = null;
		List<Status> statuses = usrTweet.getUserTimeline();
		if(id == 0 && statuses != null){
			tweet = "@" + user + ": " + statuses.get(0).getText();
			id = statuses.get(0).getId();		}
		else if (id != 0 && statuses != null){
			if(id != statuses.get(0).getId()){
				tweet = "@" + user + ": " + statuses.get(0).getText();
				id = statuses.get(0).getId();
			}				
		}
		return tweet;
	}

}
